import subprocess
import os

day = '28November2019'

for mc_production in ('Zeppelin_Mighty', ):
#for mc_production in ('Zeppelin_DL1r', 'Zeppelin_PFlow', 'Zeppelin_RNN_loose'):
    os.chdir(mc_production)
#    for sample_region in ('SR', 'CR', 'InvIsoSR', 'InvIsoCR'):
    for sample_region in ('CR', 'InvIsoSR', 'InvIsoCR'):
#    for sample_region in ('SR', ):
	
        subprocess.call("pwd")	
        
        merged_file  = "merged.{}.{}.{}.root".format(mc_production, sample_region, day)
        nominal_file = 'CalcFakeFactor/{0}/FinalPlots_{0}_mc16e.TauLH*'.format(sample_region)
#        nominal_file = 'Nominal/FinalPlots_SR_mc16e.TauLH*'
        
        subprocess.call('hadd -f {} {} '.format(merged_file, nominal_file), shell=True  )
    
    os.chdir("../")	
    
