import os
import subprocess as sp


original_dir = os.getcwd()
mia_path = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/source/'
targets = os.listdir(mia_path)
target_dirs = [d for d in targets if os.path.isdir(os.path.join(mia_path, d))]
print(target_dirs)

for target in target_dirs :
    os.chdir("{}/{}".format(mia_path, target))
    print('\n ############### {}'.format(target))
    print('{}{}'.format(mia_path, target))
    sp.call(["git", "status"])
    sp.call(["git", "diff", "origin/master", "--name-only"])

