#!/bin/sh

WORK_DIRECTORY=/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/
cd $WORK_DIRECTORY/build
setupATLAS
asetup AnalysisBase,21.2.83,here
source x*/setup.sh 
