import cv2
import os
import numpy as np
from PIL import Image as im

path = "./"

#for directory in ("LQCutFlow_EarlyRun2", "LQCutFlow"):
#    for btag in ("1", "2"):
#        img ={} 
#        for mc in ("mc16a", "mc16d", "mc16e") : 
#            file_name = "C_{}tag2pjet_0ptv_Cuts.png".format(btag)
#            img[mc] = cv2.imread( path.format(mc, directory, btag) + "/" + file_name )
#
#        list_1D = [ img["mc16a"], img["mc16d"], img["mc16e"] ]
#        cv2.imwrite("{}_{}tag.png".format(directory, btag), cv2.hconcat(list_1D))

variables = (
        "TauEta"           ,          
        "TauPhi"           ,          
        "LeadingTauPt"           ,          
        )

for directory in ("BasicKinematics_Base_ttbarCR", "BasicKinematics_Preselected_HH","BasicKinematics_Preselected_EarlyRun2","BasicKinematics_Preselected",):
    for btag in ("1","2"):
            img ={} 
            for variable in variables:
                file_name = "C_{}tag2pjet_0ptv_{}.png".format(btag, variable)
                img[variable] = cv2.imread( "plot_lq_mc16e_SR_v3/LQ3300_500_900_1300_1700/{}/{}/".format(directory, btag) + "/" + file_name )
            
#            list_var = [ 
#                    [ img["sT"]     , img["METCentrality"], img["DPhiLepMET"] ],
#                    [ img["TauEta"] , img["LeadingTauPt"] , img["DPhiLepMET"] ],
#                    [ img["MTauJet"], img["MLepJet"]      , img["M_LQhadColl_minDeltaM_1st"] ],
#                    [ img["M_LQlepColl_minDeltaM_1st"], img["QuadrantDistribution"]      ,  ],
#                ]
            list_var = [ 
                    cv2.hconcat([img["TauEta"] , img["TauPhi"], img["LeadingTauPt"]]),
                    ]
            cv2.imwrite("{}_{}.png".format(directory,btag), cv2.vconcat(list_var))

