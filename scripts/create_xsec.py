## chan      Xsec[pb]        kFact       filtereff   HistSvc_name    physically_descriptive_name
#310550	    7.91	    1.0		1.0	    LQ3Beta05	    aMcAtNloPy8EG_OffDiag_LQu_ta_ld_0p3_beta_0p5_hnd_1p0_M300
#310552	    0.49	    1.0		1.0	    LQ3Beta05	    aMcAtNloPy8EG_OffDiag_LQu_ta_ld_0p3_beta_0p5_hnd_1p0_M700
#310554	    0.0117	    1.0		1.0	    LQ3Beta05	    aMcAtNloPy8EG_OffDiag_LQu_ta_ld_0p3_beta_0p5_hnd_1p0_M900
#310556	    0.000704	    1.0		1.0	    LQ3Beta05	    aMcAtNloPy8EG_OffDiag_LQu_ta_ld_0p3_beta_0p5_hnd_1p0_M1300
#310558	    0.0000614	    1.0		1.0	    LQ3Beta05	    aMcAtNloPy8EG_OffDiag_LQu_ta_ld_0p3_beta_0p5_hnd_1p0_M1700

import pandas as pd
import numpy as np


for mc in ("mc16a", "mc16d", "mc16e") : 
    df_csv = pd.read_csv("./job/spreadsheet_{}_updated.csv".format(mc), sep=";")
    
    for iLine in range(0,len(df_csv)):
    
        Xsec       = str(df_csv.at[iLine, "Cross section [pb]"])
        JobOptions = (df_csv.at[iLine, "JobOptions"]).split(".")
        filtereff  = str(df_csv.at[iLine, "Filter efficiency"])
        
        if "LQd" in JobOptions[2] : continue
    
        print("{}\t{}\t1.0\t{}\tLQ3Beta05\t{}".format(JobOptions[1], Xsec, filtereff, JobOptions[2]))
