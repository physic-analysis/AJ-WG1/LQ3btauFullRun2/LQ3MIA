import os
import ROOT as R

grl = R.TFile.Open("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/20190707/ilumicalc_histograms_None_348885-364292_Susy_AllGood_DisappearingTrack_OflLumi-13TeV-010.root")

#zeppelin_dirs = os.listdir("/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/SR/data")
zeppelin_dirs = os.listdir("/eos/atlas/unpledged/group-tokyo/users/ktakeda/Data/HHbbll/data/")

for directory in zeppelin_dirs:
    found = False

    if ".sh"  in directory : continue
    if ".txt" in directory : continue
    
    #print(directory.split("."))

    zeppelin_run_number = directory.split(".")[3]
    zeppelin_run_number = zeppelin_run_number[2:]
    
    for th1f in grl.GetListOfKeys():
        if "intlumi" not in th1f.GetName(): continue
        
        good_run = th1f.GetName().split("_")[0][3:]
        # print(good_run)
        if zeppelin_run_number == good_run:  found = True

    if not found : 
        print(zeppelin_run_number)

