import ROOT as R

infile = R.TFile.Open("./Zeppelin_Mighty/CalcFakeFactor/SR/FinalPlots_SR_mc16e.TauLH.data.0.root")

hist = infile.Get("LQYield/data_2tag2pjet_0ptv_LumiYields")

for iBin in range(1,hist.GetNbinsX()):
    if hist.GetBinContent(iBin) != 0.0 : continue
    print(iBin)
