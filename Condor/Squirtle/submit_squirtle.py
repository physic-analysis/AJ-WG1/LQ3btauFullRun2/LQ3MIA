import os
import subprocess 
import argparse

fullpath = '/data/data2/zp/ktakeda/MIA/'
current_dir = os.getcwd()

#for Region in ('SR','CR','InvIsoSR','InvIsoCR'):
for Region in ('SR',):
#    for mc_production in ('mc16a', 'mc16d', 'mc16e'):
    for mc_production in ('mc16d', 'mc16e'):
	mcdir = 'Squirtle'
	directory = fullpath + '/' + mcdir + '/' + mc_production  + '/' + Region
	os.chdir(directory)	
	
	print "Current directory : " + os.getcwd()
	jobfile = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3MIA/Condor/Squirtle/Condor_{0}_{1}_lxatut.sub'.format(Region,  mc_production)
	print jobfile
	subprocess.call(['condor_submit', '-q', 'group_prod3', jobfile])
    
os.chdir(current_dir) 
