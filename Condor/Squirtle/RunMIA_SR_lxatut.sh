#!/bin/sh

if [ $1 = "mc16a" ] ; then
    DATA="DATA16"
    YEAR=2016
    OUTPUT_FILE_NAME="FinalPlots_SR_mc16a.root"
elif [ $1 = "mc16d" ] ; then
    DATA="DATA17"
    YEAR=2017
    OUTPUT_FILE_NAME="FinalPlots_SR_mc16d.root"
elif [ $1 = "mc16e" ] ; then
    DATA="DATA18"
    YEAR=2018
    OUTPUT_FILE_NAME="FinalPlots_SR_mc16e.root"
fi

SAMPLE_ON_EOS_MC16E_SR="root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Squirtle/${DATA}/SR/"
SAMPLE_ON_EOS_MC16E_CR="root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Squirtle/${DATA}/CR/"
SAMPLE_ON_EOS_MC16E_INV_SR="root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Squirtle/${DATA}/Inv/SR/"
SAMPLE_ON_EOS_MC16E_INV_CR="root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Squirtle/${DATA}/Inv/CR/"

if [ $2 = 160 ]; then
    SAMPLE_ON_EOS_MC16E_SR="root://eosatlas.cern.ch//eos/atlas/unpledged/group-tokyo/users/ktakeda/Data/Leptoquark/LepHad/Squirtle/${DATA}/"
elif [ $2 = 161 ]; then
    SAMPLE_ON_EOS_MC16E_SR="root://eosatlas.cern.ch//eos/atlas/unpledged/group-tokyo/users/ktakeda/Data/Leptoquark/LepHad/Squirtle/${DATA}/"
elif [ $2 = 162 ]; then
    SAMPLE_ON_EOS_MC16E_SR="root://eosatlas.cern.ch//eos/atlas/unpledged/group-tokyo/users/ktakeda/Data/Leptoquark/LepHad/Squirtle/${DATA}/"
fi

SAMPLE=${SAMPLE_ON_EOS_MC16E_SR}

# setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
echo ">>> setupATLAS done."

# xrootd
lsetup xrootd
echo ">>> setup xrootd done."

CurrentDir=${PWD}
cd /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/build_slc6/
asetup AnalysisBase,21.2.90,here,slc6
echo ">>> asetup done."

source x*/setup.sh 
echo ">>> source x*/setup.sh done."
cd ${CurrentDir}

FinalLQLQbbtautauLH \
  --release 31\
  -n -1\
  -s 0 \
  -d ${SAMPLE}\
  -p $2 \
  -q 0 \
  -y ${YEAR} \
  --ttbarNF 1\
  --analysisType TauLH \
  --outputName ${OUTPUT_FILE_NAME} \
  --outputNtupName MVAInput.root\
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1 \
  --fake    0 \
  --antiTau 0 \
  --invIsol 0 \
  --UsePFlowJet 1 \
  --UseDL1r 1 \
  --OnlyMVA 1 \
  --ff 1 \
  --lq3 1\
  -c Squirtle
  
  #--print_weight 1\
  #--level 5 \
  #--UseTextWeight 1 \
