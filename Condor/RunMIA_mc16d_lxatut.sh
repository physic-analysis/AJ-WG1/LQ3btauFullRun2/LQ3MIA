#!/bin/sh

SAMPLE_MC16A_LQ=/data/data2/zp/ktakeda/MIA/LQSignalSample/MayDaya/
SAMPLE_MC16D_LQ=/data/data2/zp/ktakeda/MIA/LQSignalSample/MayDayd/
SAMPLE_MC16E_LQ=/data/data2/zp/ktakeda/MIA/LQSignalSample/MayDaye/

SAMPLE_ON_EOS_MC16A_SR=root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/MayDaya/SR/
SAMPLE_ON_EOS_MC16D_SR=root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/BlueTuesday/SR_NoSys/
SAMPLE_ON_EOS_MC16E_SR=root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/MayDaye/SR/

SAMPLE=""
if [ $1 = "164" ]; then
  SAMPLE=${SAMPLE_MC16D_LQ}
else
  SAMPLE=${SAMPLE_ON_EOS_MC16D_SR}
fi

# setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
echo ">>> setupATLAS done."

# xrootd
lsetup xrootd
echo ">>> setup xrootd done."

CurrentDir=${PWD}
cd /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/build_slc6/
#asetup AnalysisBase,21.2.60,here,slc6
asetup --restore
echo ">>> asetup done."

source x*/setup.sh 
echo ">>> source x*/setup.sh done."
cd ${CurrentDir}

FinalLQLQbbtautauLH \
  --release 31\
  -n -1\
  -s 0 \
  -d ${SAMPLE}\
  -p $1 \
  -q 0 \
  -y 2017 \
  --analysisType TauLH \
  --outputName FinalPlots_mc16d.root \
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1\
  --fake 0 \
  --antiTau 0 \
  --ff 0 \
  --lq3 1\
  --outputNtupName testNtup.root\
  -c BlueTuesday
