import  glob


sample_list = (
	"ttbar.410471",
	"ttbar.410470",
	"data",
	"ZtautauB",
	"ZeeB",
	"ZtautauC",
	"ZtautauL",
	"ZeeC",
	"WtaunuB",
	"ZmumuB",
	"WmunuB",
	"ZeeL",
	"WtaunuC",
	"WtaunuL",
	"WmunuC",
	"WmunuL",
	"Ztautau",
	"WpqqWmlv",
	"ZmumuC",
	"WplvWmqq",
	"WlvZqq",
	"singletop_t",
	"WenuB",
	"VH.PwPy8EG_NNPDF3_AZNLO_ggZH125_llbb",
	"WqqZvv",
	"WqqZll",
	"WenuC",
	"singletop_s",
	"singletop_Wt",
	"singletop",
	"WenuL",
	"ZmumuL",
	"VH.PwPy8EG_NNPDF3_AZNLO_ZH125J_MINLO_llbb_VpT",
	"Zee",
	"ZqqZvv",
	"ZqqZll",
	"Zmumu",
	"ShDYmumu",
	"ShDYee",
	"ShDYtautau",
)

for region in ('SR', 'CR', 'InvIsoSR', 'InvIsoCR'):
	print('Sample region : {0}'.format(region))
	print('----------------------------------------')

	file_names = glob.glob('./Zeppelin/CalcFakeFactor/{0}/*'.format(region))
		
	for sample in sample_list:
		isMatched = False
		for file_name in file_names : 
			if sample in file_name :
				isMatched = True
		if not isMatched : 
			print('ERROR : {0}'.format(sample))
	
	print('\n')
	
