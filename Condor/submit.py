import os
import subprocess 
import argparse

# argument parser
parser = argparse.ArgumentParser(description='')
parser.add_argument('-m', '--mc'    , type=str, dest='mc_campaign', help='echo fname')
parser.add_argument('-s', '--sample', type=str, dest='sample'     , help='echo fname')
parser.add_argument('--all'   , action='store_true',          help='echo fname')

args = parser.parse_args()

mc = []
sample = []
if not args.all:
	if not args.mc_campaign or not args.sample:
	    print "Please put the correct argument"
	    exit(1)
	
	if ',' in args.mc_campaign :
	    mc = args.mc_campaign.split(',')
	else :
	    mc = args.mc_campaign.split()
	
	if ',' in args.sample :
	    sample = args.sample.split(',')
	else :
	    sample = args.sample.split()
else :
    mc = ('mc16a', 'mc16d', 'mc16e')
#    sample = ('Nominal', 'DataDrivenFakes')
    sample = ('Nominal', )


if ('mc16a' not in mc) and ('mc16d' not in mc) and ('mc16e' not in mc):
    print('Please check the mc campaign.')
    exit(1)

if ('Nominal' not in sample) and ('DataDrivenFakes' not in sample):
    print('Please check the sample directory name.')
    exit(1)

fullpath = '/data/data2/zp/ktakeda/MIA/'
current_dir = os.getcwd()

for McProduction in mc:
    for Sample in sample:
        mcdir = ''
	if   McProduction == 'mc16a': mcdir = 'MayDaya'
	elif McProduction == 'mc16d': mcdir = 'BlueTuesday' 
	elif McProduction == 'mc16e': mcdir = 'MayDaye'
	#elif McProduction == 'mc16e': mcdir = 'Zeppelin'
	directory = fullpath + '/' + mcdir + '/' + Sample
	os.chdir(directory)	

	print "Current directory : " + os.getcwd()
	jobfile	= ''
	if   Sample == "Nominal" : 
		jobfile = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3MIA/Condor/Condor_' + McProduction +'_lxatut.sub'
	elif Sample == "DataDrivenFakes" :
		jobfile = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3MIA/Condor/Condor_fakes_' + McProduction +'_lxatut.sub'
	subprocess.call(['condor_submit', '-q', 'group_prod2', jobfile])

os.chdir(current_dir) 
