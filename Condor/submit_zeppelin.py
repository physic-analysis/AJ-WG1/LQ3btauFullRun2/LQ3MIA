import os
import subprocess 
import argparse

current_dir = os.getcwd()

for zeppelin in ('Zeppelin_loose_RNN', 'Zeppelin_PFlow', 'Zeppelin_DL1r'):
    if   zeppelin == 'Zeppelin_loose_RNN' : decode_dir = 'Zeppelin_RNNLoose_EMTopoJet_MV2c10'
    elif zeppelin == 'Zeppelin_PFlow'     : decode_dir = 'Zeppelin_RNNLoose_EMPFlowJet_MV2c10'
    elif zeppelin == 'Zeppelin_DL1r'      : decode_dir = 'Zeppelin_RNNLoose_EMTopoJet_DL1r'
    
    directory = '/data/data2/zp/ktakeda/MIA/' + zeppelin + '/Nominal'
    os.chdir(directory)	
    
    print "Current directory : " + os.getcwd()
    jobfile = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3MIA/Condor/{0}/Condor_SR_mc16e_lxatut.sub'.format(decode_dir)
    subprocess.call(['condor_submit', '-q', 'group_prod2', jobfile])

os.chdir(current_dir) 
