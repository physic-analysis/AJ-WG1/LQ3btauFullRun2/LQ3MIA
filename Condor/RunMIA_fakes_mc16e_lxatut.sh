#!/bin/sh
#
SAMPLE_ON_EOS_MC16A_CR=root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/MayDaya/MayDayaCR/
SAMPLE_ON_EOS_MC16D_CR=root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/BlueTuesday/CR_NoSys/
SAMPLE_ON_EOS_MC16E_CR=root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/MayDaye/CR/
SAMPLE_ON_EOS_MC16E_CR=root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_medium_RNN/CR/

SAMPLE=${SAMPLE_ON_EOS_MC16E_CR}

# setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
echo ">>> setupATLAS done."

CurrentDir=${PWD}
cd /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/build_slc6/
#asetup AnalysisBase,21.2.60,here,slc6
asetup --restore
echo ">>> asetup done."

source x*/setup.sh 
echo ">>> source x*/setup.sh done."
cd ${CurrentDir}


FinalLQLQbbtautauLH \
  --release 31\
  -n -1\
  -s 0 \
  -d ${SAMPLE}\
  -p $1 \
  -q 0 \
  -y 2018 \
  --analysisType TauLH \
  --outputName FinalPlots_mc16e.root \
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1\
  --fake 1 \
  --fullyDataFakes 1\
  --antiTau 0 \
  --ff 0 \
  --lq3 1\
  --outputNtupName testNtup.root\
  -c Zeppelin
