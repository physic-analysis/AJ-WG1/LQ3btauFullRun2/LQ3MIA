import os
import subprocess 
import argparse

fullpath = '/data/data2/zp/ktakeda/MIA/'
current_dir = os.getcwd()

directory = fullpath + '/CxAOD_HHbbll/'
os.chdir(directory)	

print "Current directory : " + os.getcwd()

jobfile = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3MIA/Condor/CxAOD_HHbbll/Condor_TextWeight_rerun.sub'
print jobfile
subprocess.call(['condor_submit', '-q', 'group_prod2', jobfile])

jobfile = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3MIA/Condor/CxAOD_HHbbll/Condor_CxAODWeight.sub'
print jobfile
subprocess.call(['condor_submit', '-q', 'group_prod2', jobfile])
#
jobfile = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3MIA/Condor/CxAOD_HHbbll/Condor_data.sub'
print jobfile
subprocess.call(['condor_submit', '-q', 'group_prod2', jobfile])
    
os.chdir(current_dir) 
