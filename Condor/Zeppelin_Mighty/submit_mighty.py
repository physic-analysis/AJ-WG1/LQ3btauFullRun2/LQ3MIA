import os
import subprocess 
import argparse

fullpath = '/data/data2/zp/ktakeda/MIA/'
current_dir = os.getcwd()

#for Region in ('SR','CR','InvIsoSR','InvIsoCR'):
for Region in ('SR',):
	mcdir = 'Zeppelin_Mighty'
	directory = fullpath + '/' + mcdir + '/CalcFakeFactor/' + Region
	os.chdir(directory)	
	
        print "Current directory : " + os.getcwd()
	jobfile = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3MIA/Condor/Zeppelin_Mighty/Condor_{0}_mc16e_lxatut.sub'.format(Region)
	print jobfile
	subprocess.call(['condor_submit', '-q', 'group_prod2', jobfile])
        
        #jobfile = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3MIA/Condor/Zeppelin_Mighty/Condor_{0}_data_mc16e_lxatut.sub'.format(Region)
	#print jobfile
	#subprocess.call(['condor_submit', '-q', 'group_prod2', jobfile])

        #jobfile = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3MIA/Condor/Zeppelin_Mighty/Condor_{0}_ttbar_mc16e_lxatut.sub'.format(Region)
	#print jobfile
        #subprocess.call(['condor_submit', '-q', 'group_prod3', jobfile])
	#
        #jobfile = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3MIA/Condor/Zeppelin_Mighty/Condor_{0}_Zjets_mc16e_lxatut.sub'.format(Region)
	#print jobfile
	#subprocess.call(['condor_submit', '-q', 'group_prod3', jobfile])
        	

os.chdir(current_dir) 
