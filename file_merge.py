import ROOT
import glob

fullpath = '/data/data2/zp/ktakeda/MIA/'
current_dir = os.getcwd()

for McProduction in ('mc16a', 'mc16d', 'mc16e'):
	for Sample in ('Nominal','DataDrivenFakes'):
		mcdir = ''
		if   McProduction == 'mc16a': mcdir = 'MayDaya'
		elif McProduction == 'mc16d': mcdir = 'BlueTuesday' 
		elif McProduction == 'mc16e': mcdir = 'MayDaye'

		directory = fullpath + '/' + mcdir + '/' + Sample
                glob.glob(directory)

		print "Current directory : " + os.getcwd()
		jobfile	= ''
		if   Sample == "Nominal" : 
			jobfile = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3MIA/Condor/Condor_' + McProduction +'_lxatut.sub'
		elif Sample == "DataDrivenFakes" :
			jobfile = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3MIA/Condor/Condor_fakes_' + McProduction +'_lxatut.sub'
		subprocess.call(['condor_submit', '-q', 'group_std7', jobfile])

os.chdir(current_dir) 
