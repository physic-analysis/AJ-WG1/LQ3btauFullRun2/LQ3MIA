#!/bin/sh

# mc16e
SAMPLE_ON_EOS_MC16E_SR="/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/MayDaye/Syst_Files/"
SAMPLE_ON_EOS_MC16E_SR="/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/MayDaye/SR/"
SAMPLE_ON_EOS_MC16E_SR="/eos/atlas/unpledged/group-tokyo/users/ktakeda/Data/Leptoquark/LepHad/LQSignalSample/MayDaye/"
SAMPLE_ON_EOS_MC16E_CR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/CR/"
SAMPLE_ON_EOS_MC16E_INV_SR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/Inv/SR/"
SAMPLE_ON_EOS_MC16E_INV_CR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/Inv/CR/"

OUTPUT_FILE_NAME="FinalPlots_SR_mc16e.root"

SAMPLE=${SAMPLE_ON_EOS_MC16E_SR}
SAMPLE_NUMBER=165
# 2 : ttbar
# 165 : signal

FinalLQLQbbtautauLH \
  --release 31\
  -n 1000\
  -d ${SAMPLE}\
  -p ${SAMPLE_NUMBER} \
  -y 2018 \
  --analysisType TauLH \
  --outputName ${OUTPUT_FILE_NAME} \
  --outputNtupName MVANtuple_mc16e.root\
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1 \
  --fake    0 \
  --antiTau 0 \
  --invIsol 0 \
  --ff 1 \
  --lq3 1\
  -c MayDay
  
  #--print_weight 1\
  #--level 5 \
  #--UseTextWeight 1 \
  #--ttbarNF 1\
