#!/bin/sh

# mc16e
SAMPLE_ON_EOS_MC16E_SR="/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/build_slc6/SR"
SAMPLE_ON_EOS_MC16E_SR="/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/build_slc6/sample_zeppelin"
SAMPLE_ON_EOS_MC16E_SR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/SR/"
SAMPLE_ON_EOS_MC16E_CR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/CR/"
SAMPLE_ON_EOS_MC16E_INV_SR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/Inv/SR/"
SAMPLE_ON_EOS_MC16E_INV_CR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/Inv/CR/"

OUTPUT_FILE_NAME="FinalPlots_SR_mc16e.root"

SAMPLE=${SAMPLE_ON_EOS_MC16E_SR}

FinalLQLQbbtautauLH \
  --release 31\
  -n 1000\
  -s 0 \
  -d ${SAMPLE}\
  -p 2 \
  -q 0 \
  -y 2018 \
  --ttbarNF 1\
  --analysisType TauLH \
  --print_weight 1\
  --level 5 \
  --outputName ${OUTPUT_FILE_NAME} \
  --outputNtupName MVAInput_mc16e.root\
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1 \
  --fake    0 \
  --antiTau 0 \
  --invIsol 0 \
  --UsePFlowJet 1 \
  --UseDL1r 1 \
  --ff 1 \
  --lq3 1\
  --OnlyMVA 1\
  -c Zeppelin
  
  #--print_weight 1\
  #--level 5 \
  #--UseTextWeight 1 \
