#!/bin/sh

SAMPLE_MC16E_ZEPPELIN="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_medium_RNN/CR/"
SAMPLE=${SAMPLE_MC16E_ZEPPELIN}

FinalLQLQbbtautauLH \
  --release 31\
  -n -1\
  -s 0 \
  -d ${SAMPLE}\
  -p $1 \
  -q 0 \
  -y 2018 \
  --analysisType TauLH \
  --outputName FinalPlots_mc16e.root \
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1\
  --fake 1 \
  --fullyDataFakes 1\
  --antiTau 0 \
  --ff 0 \
  --lq3 1\
  -c Zeppelin
