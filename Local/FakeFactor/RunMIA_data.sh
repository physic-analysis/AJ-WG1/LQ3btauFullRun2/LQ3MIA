#!/bin/sh

SAMPLE_MC16E_ZEPPELIN_SR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/SR/"
#SAMPLE_MC16E_ZEPPELIN_SR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_loose_RNN/SR/"
#SAMPLE_MC16E_ZEPPELIN_SR="/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/MayDaya/MayDayaSR/"
SAMPLE_MC16E_ZEPPELIN_CR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_medium_RNN/CR/"
SAMPLE_MC16E_ZEPPELIN_INVISO_SR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_medium_RNN/InvIso/SR/"
SAMPLE_MC16E_ZEPPELIN_INVISO_CR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_medium_RNN/InvIso/CR/"

SAMPLE=""
OUTPUT_NAME=""
SAMPLE_NUMBER=0
YEAR=""
MC_PRODUCTION=""
if [ $1 == "SR" ]; then
    SAMPLE=${SAMPLE_MC16E_ZEPPELIN_SR}
    OUTPUT_NAME="FinalPlots_SR_mc16e.root"
elif [ $1 == "CR" ]; then
    SAMPLE=${SAMPLE_MC16E_ZEPPELIN_CR}
    OUTPUT_NAME="FinalPlots_CR_mc16e.root"
elif [ $1 == "InvIsoSR" ]; then
    SAMPLE=${SAMPLE_MC16E_ZEPPELIN_INVISO_SR}
    OUTPUT_NAME="FinalPlots_InvIsoSR_mc16e.root"
elif [ $1 == "InvIsoCR" ]; then
    SAMPLE=${SAMPLE_MC16E_ZEPPELIN_INVISO_CR}
    OUTPUT_NAME="FinalPlots_InvIsoCR_mc16e.root"
else
    echo "Usage : Please choose the correct MC production."
    echo "source RunMIA_local.sh mc16a"
    echo "source RunMIA_local.sh mc16d"
    echo "source RunMIA_local.sh mc16e"
    return
fi

FinalLQLQbbtautauLH \
  --release 31\
  -n -1\
  -s 0 \
  -d ${SAMPLE}\
  -p 1 \
  -q 0 \
  -y 2018 \
  --analysisType TauLH \
  --outputName ${OUTPUT_NAME} \
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1\
  --fake 1 \
  --antiTau 1 \
  --invIsol 1 \
  --ff 1 \
  --lq3 1\
  --cutFlow 0\
  -c Zeppelin
