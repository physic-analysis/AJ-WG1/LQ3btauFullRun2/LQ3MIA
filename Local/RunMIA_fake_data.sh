#!/bin/sh

SAMPLE_MC16A="/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/MayDaya/MayDayaSR/"
SAMPLE_MC16D="/eos/atlas/unpledged/group-tokyo/users/ktakeda/Data/Leptoquark/LepHad/LQSignalSample/MayDayd/"
SAMPLE_MC16E_ZEPPELIN="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_loose_RNN/SR/"

SAMPLE=""
OUTPUT_NAME=""
SAMPLE_NUMBER=0
YEAR=""
OUTPUT_NTUP=""
MC_PRODUCTION=""
if   [ $1 == "mc16a" ]; then
    echo "mc16a"
    SAMPLE=${SAMPLE_MC16A}
    OUTPUT_NAME="FinalPlots_mc16a.root"
    SAMPLE_NUMBER=2
    YEAR="2016"
    OUTPUT_NTUP="testNtup_mc16a.root"
    MC_PRODUCTION="MayDaya"
elif [ $1 == "mc16d" ]; then
    echo "mc16d"
    SAMPLE=${SAMPLE_MC16D}
    OUTPUT_NAME="FinalPlots_mc16d.root"
    SAMPLE_NUMBER=2
    YEAR="2017"
    OUTPUT_NTUP="testNtup_mc16d.root"
    MC_PRODUCTION="BlueTuesday"
elif [ $1 == "mc16e" ]; then
    echo "mc16e"
    SAMPLE=${SAMPLE_MC16E}
    OUTPUT_NAME="FinalPlots_mc16e.root"
    SAMPLE_NUMBER=2
    YEAR="2018"
    OUTPUT_NTUP="testNtup_mc16e.root"
    MC_PRODUCTION="MayDaye"
elif [ $1 == "mc16e_zep" ]; then
    echo "mc16e"
    SAMPLE=${SAMPLE_MC16E_ZEPPELIN}
    OUTPUT_NAME="FinalPlots_mc16e_zepppelin.root"
    SAMPLE_NUMBER=2
    YEAR="2018"
    OUTPUT_NTUP="testNtup_mc16e_zepppelin.root"
    MC_PRODUCTION="Zeppelin"
else
    echo "Usage : Please choose the correct MC production."
    echo "source RunMIA_local.sh mc16a"
    echo "source RunMIA_local.sh mc16d"
    echo "source RunMIA_local.sh mc16e"
    return
fi

FinalLQLQbbtautauLH \
  --release 31\
  -n 10000\
  -s 0 \
  -d ${SAMPLE}\
  -p 1 \
  -q 0 \
  -y ${YEAR} \
  -l 4\
  --analysisType TauLH \
  --outputName ${OUTPUT_NAME} \
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1\
  --fake 1 \
  --fullyDataFakes 1\
  --antiTau 0 \
  --ff 0 \
  --lq3 1\
  --cutFlow 0\
  --outputNtupName ${OUTPUT_NTUP}\
  -c ${MC_PRODUCTION}
