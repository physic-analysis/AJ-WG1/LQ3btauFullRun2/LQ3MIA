#!/bin/sh

# mc16e
SAMPLE_ON_EOS_MC16E_SR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_PFlow/SR/"

OUTPUT_FILE_NAME="FinalPlots_SR_mc16e.root"

SAMPLE=${SAMPLE_ON_EOS_MC16E_SR}

# setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
echo ">>> setupATLAS done."

# xrootd
lsetup xrootd
echo ">>> setup xrootd done."

CurrentDir=${PWD}
cd /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/build_slc6/
#asetup AnalysisBase,21.2.60,here,slc6
asetup --restore
echo ">>> asetup done."

source x*/setup.sh 
echo ">>> source x*/setup.sh done."
cd ${CurrentDir}

FinalLQLQbbtautauLH \
  --release 31\
  -n 100\
  -s 0 \
  -d ${SAMPLE}\
  -p 2 \
  -q 0 \
  -y 2018 \
  --analysisType TauLH \
  --outputName ${OUTPUT_FILE_NAME} \
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1 \
  --fake    0 \
  --antiTau 0 \
  --invIsol 0 \
  --UsePFlowJet 0\
  --UseDL1r 1\
  --ff 1 \
  --lq3 1\
  -c Zeppelin
