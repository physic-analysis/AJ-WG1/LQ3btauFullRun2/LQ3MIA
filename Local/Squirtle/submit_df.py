import os
import subprocess 
import argparse

fullpath = '/data/data2/zp/ktakeda/MIA/'
current_dir = os.getcwd()

for McProduction in ('mc16e',):
    for Region in ('CR',):
        mcdir = 'Zeppelin'
    	directory = fullpath + '/' + mcdir + '/DataDrivenFakes/'
    	os.chdir(directory)	
    
    	print "Current directory : " + os.getcwd()
    	jobfile = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3MIA/Condor/FakeFactor/Condor_DataDrivenFakes_mc16e.sub'
	print jobfile
    	subprocess.call(['condor_submit', '-q', 'group_prod2', jobfile])
    
os.chdir(current_dir) 
