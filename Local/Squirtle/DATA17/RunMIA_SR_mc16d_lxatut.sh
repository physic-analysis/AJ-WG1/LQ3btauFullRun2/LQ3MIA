#!/bin/sh

# mc16e
SAMPLE_ON_EOS_MC16E_SR="/eos/atlas/unpledged/group-tokyo/users/ktakeda/Data/Leptoquark/LepHad/Squirtle/DATA17/"
SAMPLE_ON_EOS_MC16E_SR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Squirtle/DATA17/SR/"
SAMPLE_ON_EOS_MC16E_CR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/CR/"
SAMPLE_ON_EOS_MC16E_INV_SR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/Inv/SR/"
SAMPLE_ON_EOS_MC16E_INV_CR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/Inv/CR/"

OUTPUT_FILE_NAME="FinalPlots_SR_mc16e.root"

SAMPLE=${SAMPLE_ON_EOS_MC16E_SR}

FinalLQLQbbtautauLH \
  --release 31\
  -n 10000\
  -s 0 \
  -d ${SAMPLE}\
  -p 200 \
  -q 0 \
  -y 2017 \
  --analysisType TauLH \
  --outputName ${OUTPUT_FILE_NAME} \
  --outputNtupName MVAInput.root\
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1 \
  --fake    0 \
  --antiTau 0 \
  --invIsol 0 \
  --UsePFlowJet 1 \
  --UseDL1r 1 \
  --ff 1 \
  --lq3 1\
  --OnlyMVA 1 \
  -c Squirtle
  
  #--print_weight 1\
  #--level 5 \
  #--UseTextWeight 1 \
