#!/bin/sh

# mc16e
SAMPLE_ON_EOS_MC16E_SR="root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/SR/"
SAMPLE_ON_EOS_MC16E_CR="root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/CR/"
SAMPLE_ON_EOS_MC16E_INV_SR="root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/Inv/SR/"
SAMPLE_ON_EOS_MC16E_INV_CR="root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/Inv/CR/"

OUTPUT_FILE_NAME="FinalPlots_InvIsoCR_mc16e.root"

SAMPLE=${SAMPLE_ON_EOS_MC16E_INV_CR}

# setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
echo ">>> setupATLAS done."

# xrootd
lsetup xrootd
echo ">>> setup xrootd done."

CurrentDir=${PWD}
cd /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/build_slc6/
#asetup AnalysisBase,21.2.60,here,slc6
asetup --restore
echo ">>> asetup done."

source x*/setup.sh 
echo ">>> source x*/setup.sh done."
cd ${CurrentDir}

#NUMBER=-1
#if [ $1 = "1" ]; then
#  NUMBER=430000
#  NUMBER=280000
#fi

FinalLQLQbbtautauLH \
  --release 31\
  -n -1\
  -s 0 \
  -d ${SAMPLE}\
  -p $1 \
  -q 0 \
  -y 2018 \
  --analysisType TauLH \
  --outputName ${OUTPUT_FILE_NAME} \
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1 \
  --fake    1 \
  --antiTau 1 \
  --invIsol 1 \
  --ff 1 \
  --lq3 1\
  -c Zeppelin
