#!/bin/sh

# mc16e
SAMPLE_ON_EOS_MC16E_SR="/eos/atlas/unpledged/group-tokyo/users/ktakeda/Data/HHbbll/"
SAMPLE_ON_EOS_MC16E_CR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/CR/"
SAMPLE_ON_EOS_MC16E_INV_SR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/Inv/SR/"
SAMPLE_ON_EOS_MC16E_INV_CR="/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Zeppelin_Mighty/Inv/CR/"

OUTPUT_FILE_NAME="FinalPlots_SR_mc16e.root"

SAMPLE=${SAMPLE_ON_EOS_MC16E_SR}

FinalLQLQbbleplep \
  --release 31\
  -n 5000\
  -s 0 \
  -d ${SAMPLE}\
  -p 1000 \
  -q 0 \
  -y 2018 \
  -a TauLH \
  --bbleplep 1 \
  --print_weight 1\
  --level 5 \
  --outputName ${OUTPUT_FILE_NAME} \
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1 \
  --fake    0 \
  --antiTau 0 \
  --invIsol 0 \
  --UsePFlowJet 1 \
  --UseDL1r 1 \
  --ff 1 \
  --lq3 1\
  -c HHbbll
  
#  --print_weight 1\
#  --level 5 \
#  --UseTextWeight 1 \
