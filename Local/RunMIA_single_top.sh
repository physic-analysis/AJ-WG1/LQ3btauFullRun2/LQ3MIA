#!/bin/sh

SAMPLE_MC16A="/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/MayDaya/MayDayaSR/"
SAMPLE_MC16D="/eos/atlas/unpledged/group-tokyo/users/ktakeda/Data/Leptoquark/LepHad/LQSignalSample/MayDayd/"
SAMPLE_MC16E="/eos/atlas/unpledged/group-tokyo/users/ktakeda/Data/Leptoquark/LepHad/LQSignalSample/MayDaye/"

SAMPLE=""
OUTPUT_NAME=""
SAMPLE_NUMBER=0
YEAR=""
OUTPUT_NTUP=""
CxAOD_Production=""
if   [ $1 == "mc16a" ]; then
    echo "mc16a"
    SAMPLE=${SAMPLE_MC16A}
    OUTPUT_NAME="FinalPlots_mc16a.root"
    SAMPLE_NUMBER=3
    YEAR="2016"
    OUTPUT_NTUP="testNtup_mc16a.root"
    CxAOD_Production="MayDaya"
elif [ $1 == "mc16d" ]; then
    echo "mc16d"
    SAMPLE=${SAMPLE_MC16D}
    OUTPUT_NAME="FinalPlots_mc16d.root"
    SAMPLE_NUMBER=3
    YEAR="2017"
    OUTPUT_NTUP="testNtup_mc16d.root"
    CxAOD_Production="BlueTuesday"
elif [ $1 == "mc16e" ]; then
    echo "mc16e"
    SAMPLE=${SAMPLE_MC16E}
    OUTPUT_NAME="FinalPlots_mc16e.root"
    SAMPLE_NUMBER=3
    YEAR="2018"
    OUTPUT_NTUP="testNtup_mc16e.root"
    CxAOD_Production="MayDaye"
else
    echo "Usage : Please choose the correct MC production."
    echo "source RunMIA_local.sh mc16a"
    echo "source RunMIA_local.sh mc16d"
    echo "source RunMIA_local.sh mc16e"
    return
fi

FinalLQLQbbtautauLH \
  --release 31\
  -n -1\
  -s 0 \
  -d ${SAMPLE}\
  -p ${SAMPLE_NUMBER} \
  -q 0 \
  -y ${YEAR} \
  --analysisType TauLH \
  --outputName ${OUTPUT_NAME} \
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1\
  --fake 0 \
  --antiTau 0 \
  --ff 0 \
  --lq3 1\
  --cutFlow 0\
  --outputNtupName ${OUTPUT_NTUP} \
  -c ${CxAOD_Production}
