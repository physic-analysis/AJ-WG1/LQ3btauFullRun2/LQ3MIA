import subprocess

# ./
# `- Run 
#     |- MayDaya     (mc16a)
#     |     |- Nominal
#     |     `- DataDrivenFakes
#     |- BlueTuesday (mc16d)
#     |     |- Nominal
#     |     `- DataDrivenFakes
#     `- MayDaye     (mc16e)
#           |- Nominal
#           `- DataDrivenFakes

for McProduction in ('MayDaya', 'BlueTuesday', 'MayDaye', 'Zeppelin','Zeppelin_Mighty','Zeppelin_loose_RNN','Zeppelin_PFlow','Zeppelin_DL1r'):
    for Sample in ('Nominal','DataDrivenFakes'):
        directory = McProduction + '/' + Sample
        subprocess.call(['mkdir', '-p', directory])
    
    for Sample in ('CalcFakeFactor',):
	for Region in ('SR','CR','InvIsoSR','InvIsoCR'):
		directory = McProduction + '/' + Sample + '/' + Region
        	subprocess.call(['mkdir', '-p', directory])

for period in ('mc16a', 'mc16d', 'mc16e'):
    for region in ('SR',):
        directory = 'Squirtle/{0}/{1}'.format(period, region)
        subprocess.call(['mkdir', '-p', directory])
